import { DataTypes, Sequelize} from 'sequelize';

const sequelize = new Sequelize('app_db', 'app_user', 'secretpassword', {
    host: 'postgres',
    port: 5432,
    dialect: 'postgres'
  });

export  const Comments = sequelize.define('Comments',{
    id:{
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    text:{
        type: DataTypes.STRING,
    }
},{
  freezeTableName: true,
});