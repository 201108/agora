import { DataTypes, Sequelize} from 'sequelize';

const sequelize = new Sequelize('app_db', 'app_user', 'secretpassword', {
    host: 'postgres',
    port: 5432,
    dialect: 'postgres'
  });

export const Users = sequelize.define('Users',{
    id:{
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    pseudo:{
        type: DataTypes.STRING,
        allowNull: false
    },
    email:{
        type: DataTypes.STRING,
        allowNull: false
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false
    }
},{
    freezeTableName: true,
});