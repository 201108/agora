import { DataTypes, Sequelize} from 'sequelize';


const sequelize = new Sequelize('app_db', 'app_user', 'secretpassword', {
    host: 'postgres',
    port: 5432,
    dialect: 'postgres'
  });

export  const Proposition = sequelize.define('Propositions',{
    id:{
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    name:{
        type: DataTypes.STRING,
        allowNull: false
    },
    status:{
      type: DataTypes.STRING,
      allowNull: false
  },
  // TODO we will certainly need to change the data type
  content:{
    type: DataTypes.BLOB,
    allowNull: false
}
},{
  freezeTableName: true,
});

export const PropositionParty = sequelize.define(
  "PropositionParty",
  {},
  { timestamps: false }
);

export const PropositionCreators = sequelize.define(
  "PropositionCreators",
  {},
  { timestamps: false }
);