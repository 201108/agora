import { gql } from "apollo-server-express"; 
const Schema = gql`
  type  Proposition {
    id: ID!
    name: String
  }
  type Query {
    getAllProposition: [Proposition]
    getProposition(id: String): Proposition
  }
`;
export default Schema; 