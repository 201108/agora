import {Sequelize, DataTypes} from 'sequelize';
import { Comments } from './db/CommentEntity';
import { PoliticalParty } from './db/PoliticalParty';
import { Politician } from './db/Politician';
import { Proposition, PropositionCreators, PropositionParty } from './db/Proposition';
import { Rating } from './db/Rating';
import { Users } from './db/UserEntity';


export const sequelize = new Sequelize('app_db', 'app_user', 'secretpassword', {
    host: 'postgres',
    port: 5432,
    dialect: 'postgres'
  });

async function test(){
     var u1 = await Users.create({
        pseudo:'usertest',
        password:'mysupersecretpass',
        email:'usertest@gmail.com' 
    });
    
    
    const c1=await Comments.create({  
        Users: u1,
        text:'Vraiment un super article'
    });

    const c2=await Comments.create({
        Users: u1,
        text:'Pire article que je n\'ai jamais lue'
    });


    c2.destroy();
    c1.destroy();
    u1.destroy();

}

async function initDatabase(){
    
    try {
        await sequelize.authenticate();
    console.log('Connection to database has been established successfully.');
    } catch (error) {
        console.error('Unable to connect to the database:', error);
    }


    await Users.sync({alter: true});
      await Comments.sync({alter: true});
      await Politician.sync({alter: true});
      await Proposition.sync({alter: true});
      await PoliticalParty.sync({alter: true});
      await Rating.sync({alter: true});
      await sequelize.sync({alter: true})


    Users.hasMany(Rating);
    Users.hasMany(Comments);

    Comments.belongsTo(Users, {
        foreignKey: {
          allowNull: false
        }
      });
      Comments.belongsTo(Proposition, {
        foreignKey: {
          allowNull: true
        }
      });

      PoliticalParty.hasMany(Politician);
      PoliticalParty.belongsToMany(Proposition, {through: PropositionParty});
    
      Politician.belongsToMany(Proposition,{through: PropositionCreators});
      Politician.belongsTo(PoliticalParty);

      Proposition.belongsToMany(PoliticalParty, {through: PropositionParty})
      Proposition.belongsToMany(Politician,{through: PropositionCreators});
      Proposition.hasMany(Comments);
      Proposition.hasMany(Rating)

      Rating.belongsTo(Users, {
        foreignKey: {
          allowNull: false
        }
      });
      Rating.belongsTo(Proposition, {
        foreignKey: {
          allowNull: false
        }
      });

      await sequelize.sync({alter: true})
      await Users.sync({alter: true});
      await Comments.sync({alter: true});
      await Politician.sync({alter: true});
      await Proposition.sync({alter: true});
      await PoliticalParty.sync({alter: true});
      await Rating.sync({alter: true});
      await PropositionCreators.sync({alter: true});
      await PropositionParty.sync({alter: true});
      

    console.log("Database inited");
    await test();    
    console.log("Test checked");
}

export default initDatabase;