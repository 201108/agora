import { Pool } from 'pg';

const pool = new Pool({
    user: 'app_user',
    host: 'postgres',
    database: 'app_db',
    password: 'secretpassword',
    port: 5432,
});

pool.on('error', (err, client) => {
    console.error('Error:', err);
});

export default pool;