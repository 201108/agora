import proposition, { getProposition } from "./dataset";
const Resolvers = {
  Query: {
    getAllProposition: () => getProposition("aaa"), 
    getProposition: (_: any, args: any) => { 
      console.log(args);
      return proposition.find((proposition) => proposition.id === args.id);
    },
  },
};
export default Resolvers;